import React from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'
import auth from "@react-native-firebase/auth"
import {  useNavigation } from '@react-navigation/native';

const Home = () => {
    const navigation = useNavigation();
    const signOut = () => {
        auth()
        .signOut()
        .then(() => navigation.navigate("Login"));
    }

  return (
    <View style = {styles.screen}>
        <Text style = {styles.title}>you have successfully logged in</Text>
        <View style={styles.buttons}>
        <Button title="log out"  onPress={signOut}/>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
    screen: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 21,
      marginBottom: 30,
    },
    input: {
      width: 300,
      borderRadius: 6,
      borderWidth: 2,
      borderColor: '#6d69c3',
      marginVertical: 10,
      padding: 15,
    },
    buttons: {
      width: 150,
      marginTop: 30,
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
  });

export default Home