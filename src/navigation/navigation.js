import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from '../screens/Login';
import Home from '../screens/Home';
import { SignUp } from '../screens/SignUp';

const Main = createNativeStackNavigator();

export default function Navigation() {
  return (
    <NavigationContainer>
      <Main.Navigator
      initialRouteName="Login"
      screenOptions={{ headerShown: false, animationEnabled: false }}
      headerMode="none"
      >
        <Main.Screen name="Login" component={Login} />
        <Main.Screen name="Home" component={Home} />
        <Main.Screen name="SignUp" component={SignUp} />
      </Main.Navigator>
    </NavigationContainer>
  );
}
{/* <NavigationContainer>
<Stack.Navigator>
  <Stack.Screen name="Home" component={HomeScreen} />
</Stack.Navigator>
</NavigationContainer> */}